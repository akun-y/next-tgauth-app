import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { ChakraProvider, extendTheme } from "@chakra-ui/react"
import colors from '../theme/colors'


export default function App({ Component, pageProps }: AppProps) {
  // 1. Import `extendTheme`


  // 2. Call `extendTheme` and pass your custom values
  const theme = extendTheme({
    colors,
  })


  return (
    <ChakraProvider theme={theme}>
      <Component {...pageProps} />
    </ChakraProvider>
  )
}
